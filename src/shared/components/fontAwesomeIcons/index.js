import { library } from '@fortawesome/fontawesome-svg-core';
import { faShoppingCart, faShip } from '@fortawesome/free-solid-svg-icons';

library.add(faShoppingCart, faShip);
