import React, {useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { deepOrange, deepPurple } from '@material-ui/core/colors';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {GlobalContext} from './../context/GlobalProvider';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}));

export default function LetterAvatars() {
    const classes = useStyles();
    const { cartProducts} = useContext(GlobalContext);
    return (
        <div className={classes.root}>
            <Avatar className={classes.orange}>
                <FontAwesomeIcon icon='shopping-cart' />
            </Avatar>
            <span style={{ margin: 0 }}>{cartProducts.length}</span>
        </div>
    );
}
