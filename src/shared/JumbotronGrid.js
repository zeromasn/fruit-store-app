import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Jumbotron from './JumbotronSlider';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    textContainer: {
        bottom: '0px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

const CenteredGrid = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={3} className={classes.textContainer}>
                    <Typography
                        style={{
                            fontFamily: 'fantasy',
                            fontSize: '20px',
                        }}>
                        <h1>Today's Hot Deal</h1>
                    </Typography>
                    <span>
                        <p>
                            <i
                                style={{
                                    fontSize: '16px',
                                }}>
                                For More Deals
                            </i>
                            <Button href='#text-buttons' color='primary'>
                                <Typography
                                    style={{
                                        fontFamily: 'fantasy',
                                        fontSize: '40px',
                                    }}>
                                    {' '}
                                    Click Me
                                </Typography>
                            </Button>
                        </p>
                    </span>
                </Grid>
                <Grid item xs={9} className={classes.paper}>
                    {/* <Paper className={classes.paper}>xs=6</Paper> */}
                    <Jumbotron />
                </Grid>
            </Grid>
        </div>
    );
};

export default CenteredGrid;
