import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ButtonGroup from './ButtonGroup';
// import InboxIcon from '@material-ui/icons/Inbox';
// import GroupedButtons from './GroupedButton';

const CartListItems = (props) => {
    // return (
    //     <div>
    //         <ListItem button>
    //             {/* <ListItemIcon>
    //                 <InboxIcon />
    //             </ListItemIcon> */}
    //             <ListItemText>{props.name}</ListItemText>
    //         </ListItem>
    //         <Divider />
    //     </div>
    // );

    return (
        <div>
            <ListItem button>
                {/* <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon> */}
                <ListItemText primary='Inbox' />
                <ButtonGroup />
            </ListItem>
            <Divider />
            <ListItem button>
                {/* <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon> */}
                <ListItemText primary='Inbox' />
                <ButtonGroup />
            </ListItem>
        </div>
    );
};

export default CartListItems;

// function ListItemLink(props) {
//     return <ListItem button component='a' {...props} />;
// }
