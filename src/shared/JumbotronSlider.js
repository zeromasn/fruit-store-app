import React, { useEffect, useState } from 'react';

import Slider from 'infinite-react-carousel';
import Images from '../static/exportStatic';

const Jumbotron = () => {
    return (
        <Slider autoplay autoplaySpeed={3000} duration={500} arrows={false}>
            {Images &&
                Images.map((image) => {
                    console.log(image);
                    return (
                        <div>
                            <a href='/'>
                                <img
                                    alt='stack overflow'
                                    src={image.default}></img>
                            </a>
                        </div>
                    );
                })}
        </Slider>
    );
};

export default Jumbotron;
