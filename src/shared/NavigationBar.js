// core react import
import React from 'react';

// material and component modules
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import { Link } from 'react-router-dom';
// external css imports
import './NavigationBar.css';
import CartIcon from './CartIcon';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        float: 'right',
        position: 'relative',
    },
    title: {
        flexGrow: 1,
    },
}));

export default function DenseAppBar() {
    const classes = useStyles();

    return (
        <div>
            <AppBar position='static'>
                <Toolbar variant='dense'>
                    <Typography variant='h6' className={classes.title}>
                        <Link to='/' className='app-bar__title'>
                            Happy Fruits
                        </Link>
                    </Typography>
                    <Link exact to='/cart' className='app-bar__title'>
                        <CartIcon />
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    );
}
