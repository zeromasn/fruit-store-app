import React, {useContext } from 'react';
import CartList from '../shared/CartList';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import BillingSection from '../shared/BillingSection';
import { Link } from '@material-ui/core';
import { GlobalContext } from './../context/GlobalProvider';

const Cart = (props) => {
    const { cartProducts } = useContext(GlobalContext);

    if (cartProducts.length === 0) {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                }}>
                <h1>
                    Your cart is empty. Try adding things to cart&nbsp;
                    <Link href='/'>Home Page</Link>
                </h1>
            </div>
        );
    }

    return (
        <div className='App' style={{marginTop: 10}}>
            <CssBaseline />
            <Container
                minWidth='md'
                style={{
                    marginBottom: '12px',
                    display: 'flex',
                    flexDirection: 'row',
                }}>
                <div style={{ flexGrow: '3' }}>
                    <CartList />
                </div>

                <div style={{ flexGrow: '1', marginLeft: '20px'}}>
                    <BillingSection />
                </div>
            </Container>
        </div>
    );
};

export default Cart;
