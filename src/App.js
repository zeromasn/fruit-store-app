import React, { useEffect, useState } from 'react';
import './shared/components/fontAwesomeIcons';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import HomeComponent from './pages/LandingPage';
import DenseAppBar from './shared/NavigationBar';
import Cart from './pages/Cart';
import { GlobalProvider } from './context/GlobalProvider';

function App() {

    return (
        <React.Fragment>
            <GlobalProvider>
                <Router>
                    <DenseAppBar />
                    <Route exact path='/' component={HomeComponent} /> 
                    <Route exact path='/cart' component={Cart} />
                </Router>
            </GlobalProvider>
        </React.Fragment>
    );
}

export default App;
